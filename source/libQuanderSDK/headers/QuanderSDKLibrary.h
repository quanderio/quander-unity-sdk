//
//  QuanderSDKLibrary.h
//  QuanderSDKLibrary
//
//  Created by Sean Howard on 12/10/2015.
//  Copyright © 2015 Sean Howard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quander.h"

@interface QuanderSDKLibrary : NSObject
{
    char GameObject[256];
}
char* cStringCopy(const char* string);
static NSString* CreateNSString(const char* string);
+ (char *)stringFromDataObject:(id)object;

extern "C" {
    void quanderRegisterForNotificationsWithGameObject(char *gameObject);
    
    /**
     *  quanderAttemptAutoLogin() UnitySendMessage Callbacks
     *  
     *  quanderSuccessfullyLoggedIn: string
     *  quanderFailedToLoginWithError: string
     *  quanderDidGetAllProjects: string
     *  quanderDidFailToGetProjects: string
     *
     */
    void quanderAttemptAutoLogin();
    
    /**
     *  quanderLoginWithCredentials(char *username, char *password) UnitySendMessage Callbacks
     *
     *  quanderSuccessfullyLoggedIn: string
     *  quanderFailedToLoginWithError: string
     *  quanderDidGetAllProjects: string
     *  quanderDidFailToGetProjects: string
     *
     */
    void quanderLoginWithCredentials(char *username, char *password);
    
    /**
     *
     *  quanderSubmissionWithMediaURLAndPosterURLAndAttendeeJSONOfMediaTypeWithProjectUUIDInTestMode UnitySendMessage Callbacks
     *
     *  quanderSuccessfullySubmittedAttendee: string
     *  quanderAttendeeSubmissionFailedWithError: string
     */
    void quanderSubmissionWithMediaURLAndPosterURLAndAttendeeJSONOfMediaTypeWithProjectUUIDInTestMode(char *fileURL, char *posterURL, char *attendeeJson, char *mediaType, char *projectUuid, int testMode);
    
    /**
     *  No UnitySendMessageCallbacks
     */
    int quanderRequestPendingUploads();
    
    /**
     *  quanderForcePendingUploads() UnitySendMessage Callbacks
     *
     *  quanderForceUploadsCallback: string
     */
    void quanderForcePendingUploads();
    
    /**
     *  quanderLogout() UnitySendMessage Callbacks
     *
     *  quanderSuccessfullyLoggedOut: string
     *  quanderFailedToLogoutWithError: string
     *
     */
    void quanderLogout();
}

@end
