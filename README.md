# Quander SDK

The QuanderSDK static library is built to allow Unity developers to upload images and videos, automatically send emails and view analytics using [Quander](http://quander.io). 

## How To Get Started

### Static Library Dependencies \[**manual**\]

The QuanderSDK requires the following dependencies to be imported into the Xcode project:

- [AFNetworking](https://github.com/AFNetworking/AFNetworking)
- [AWSS3](https://github.com/aws/aws-sdk-ios)
- [MDMCoreData](https://github.com/mmorey/MDMCoreData)
- [SDWebImage](https://github.com/rs/SDWebImage)

And will require the following frameworks to be linked with the project:

- MobileCoreServices.framework
- CoreGraphics.framework
- Security.framework
- SystemConfiguration.framework

### Static Library Dependencies with CocoaPods \[**recommended**\]

[CocoaPods](http://cocoapods.org) is a dependency manager for Objective-C, which automates and simplifies the process of using 3rd-party libraries in your projects. See the [“Getting Started” guide for more information](https://bitbucket.org/quanderio/quander-unity-sdk/wiki/Cocoapods%20Getting%20Started).

It’s recommended to read Step 3 in the getting started guide to learn how to edit specific build settings to get CocoaPods to play friendly with the Unity exported ```.xcodeproj``` file.

#### Podfile
```
source 'https://github.com/Cocoapods/Specs.git'

inhibit_all_warnings!

platform :ios, '9.0'

pod 'AFNetworking'
pod 'AWSS3'
pod 'MDMCoreData/MDMPersistenceController'
pod 'SDWebImage'
```

### NSAppTransportSecurity

Since the release of iOS 9 developers now have adhere to the NSAppTransportSecurity protocol.

> App Transport Security is a feature that improves the security of connections between an app and web services. The feature consists of default connection requirements that conform to best practices for secure connections. Apps can override this default behavior and turn off transport security. - Apple
[Read More](https://developer.apple.com/library/prerelease/ios/technotes/App-Transport-Security-Technote/) 

This means to communicate with the Quander API you need inform App Transport Security that you want to authorise your app to make communications to the Quander domain.

As you may want to download assets from external URLs, make uploads to AWS, not just communicate with the Quander API, you can turn off NSAppTransportSecurity be adding the following parameters to your app’s ```Info.plist```:

```
<key>NSAppTransportSecurity</key>
<dict>
  <key>NSAllowsArbitraryLoads</key>
      <true/>
</dict>
```