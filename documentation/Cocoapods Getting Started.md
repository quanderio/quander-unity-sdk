# Getting started with QuanderSDK static library with CocoaPod dependencies

This is a guide to help developers get the Quander SDK static library integrated and running with their Unity iOS Xcode project.

These step-by-step instructions are written for Xcode 7 on Mac OS X El Capitan, using the iOS 9 SDK.

> Credit to the [```AFNetworking```](https://github.com/AFNetworking/AFNetworking) team for putting the original version of this guide together which we’ve edited for our purposes.

## Step 1: Download CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Objective-C, which automates and simplifies the process of using 3rd-party libraries in your projects.

CocoaPods is distributed as a ruby gem, and is installed by running the following commands in Terminal.app:

```
$ sudo gem install cocoapods
$ pod setup
```

If using Mac OS X El Capitan you may need install CocoaPods using the following command instead in Terminal.app:

```
$ sudo gem install -n /usr/local/bin cocoapods
$ pod setup
```

## Step 2: Create a Podfile
Project dependencies to be managed by CocoaPods are specified in a file called ```Podfile```. Create this file in the same directory as your Xcode project (```.xcodeproj```) file:

```
$ touch Podfile
$ open -a Xcode Podfile
```

You just created the pod file and opened it using Xcode! Ready to add some content to the empty pod file?

Copy and paste the following lines into the Xcode window or your preferred text editor:

```
source 'https://github.com/Cocoapods/Specs.git'

inhibit_all_warnings!

platform :ios, '9.0'

pod 'AFNetworking'
pod 'AWSS3'
pod 'MDMCoreData/MDMPersistenceController'
pod 'SDWebImage'
```

## Step 3: Edit ```.xcodeproj``` build settings

When exporting your Xcode project from Unity some build settings are automatically set in the project by Unity. Some of which stop CocoaPods dependencies from working together on installation. So we’ll need amend these before installing our dependencies.

All of the settings we’re going to change can be found when opening the ```.xcodeproj``` generated from Unity in Xcode.

- Select your project from the file navigator on the left panel.

![](https://bytebucket.org/quanderio/quander-unity-sdk/raw/64364f722315f36be701d413a11d1b12bc62cdb2/documentation/images/step1.png)

- Select your build target from the panel to the right of the file navigator.

![](https://bytebucket.org/quanderio/quander-unity-sdk/raw/64364f722315f36be701d413a11d1b12bc62cdb2/documentation/images/step2.png)

- Select ‘Build Settings’ from the toolbar.

![](https://bytebucket.org/quanderio/quander-unity-sdk/raw/64364f722315f36be701d413a11d1b12bc62cdb2/documentation/images/step3.png)

- Using the search bar underneath the toolbar search for ‘Other Linker Flags’. Double click the second column, click the plus button on the resulting popover and add ```$(inherited)``` in the new row. Hit enter. Dismiss the popover by clicking anywhere else in the window.

![](https://bytebucket.org/quanderio/quander-unity-sdk/raw/64364f722315f36be701d413a11d1b12bc62cdb2/documentation/images/step4.png)

- Follow the same steps as above searching for ‘Header Search Paths’ and ‘Other C Flags’. The results should be similar to below:

![](https://bytebucket.org/quanderio/quander-unity-sdk/raw/64364f722315f36be701d413a11d1b12bc62cdb2/documentation/images/step5.png)

![](https://bytebucket.org/quanderio/quander-unity-sdk/raw/64364f722315f36be701d413a11d1b12bc62cdb2/documentation/images/step6.png)

## Step 4: Install Dependencies

Now you’re ready to install the dependencies in your project. Navigate to the same directory as your project (```.xcodeproj```) file in Terminal.app. Run the following command:

```
$ pod install
```

> If you see any build warnings appear in the Terminal mentioning either ‘Other Linker Flags’, ‘Other C Flags’ or ‘Header Search Paths’ look back at step 3 and double check you’ve edited your project settings correctly. If you’re unsure, re-export the project from Unity again, replacing all previous exports. 

From now on, be sure to always open the generated Xcode workspace (```.xcworkspace```) instead of the project file when building your project: 

```
$ open <YourProjectName>.xcworkspace
```

## Step 5: You’re good to go 👍

From the workspace Build and Run your project!





